/**
	* interface PuedeCircular
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
interface PuedeCircular {
	public void circular();
}