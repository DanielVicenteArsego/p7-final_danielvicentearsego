/**
	* interface PuedeNavegar
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
interface PuedeNavegar {
	public void navegar();
}