import javax.swing.JOptionPane;
/**
	* Clase Motocicleta
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public class Motocicleta extends Vehiculo implements PuedeCircular {
	private int potenciaMotor;
	private boolean tieneMaletero;
	
	/**
	* Constructor por defecto
	*/
	public Motocicleta() {
		super();
		setPotenciaMotor(200);
		setTieneMaletero("NO");
	}
	
	/**
	* Constructor con parametros
	* @param aux_marca
	* @param aux_modelo
	* @param @aux_color
	* @param aux_kilometros
	* @param aux_numPuertas
	* @param aux_numPlazas
	* @param aux_potenciaMotor
	* @param aux_tieneMaletero
	*/
	public Motocicleta(String aux_marca, String aux_modelo, String aux_color, double aux_kilometros, int aux_numPuertas, int aux_numPlazas, int aux_potenciaMotor, String aux_tieneMaletero) {
		super(aux_marca, aux_modelo, aux_color, aux_kilometros, aux_numPuertas, aux_numPlazas);
		setPotenciaMotor(aux_potenciaMotor);
		setTieneMaletero(aux_tieneMaletero);
	}
	
	/**
	* @param aux_potenciaMotor = potenciaMotor
	*/
	public void setPotenciaMotor(int aux_potenciaMotor) {
		if (aux_potenciaMotor < 0 || aux_potenciaMotor > 10000) {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			potenciaMotor = 200;
		}
		else {
			potenciaMotor = aux_potenciaMotor;
		}
	}
	
	/**
	* @return potenciaMotor
	*/
	public int getPotenciaMotor() {
		return potenciaMotor;
	}
	
	/**
	* @param aux_tieneMaletero = tieneMaletero
	*/
	public void setTieneMaletero(String aux_tieneMaletero) {
		if (aux_tieneMaletero.toUpperCase().equals("NO")) {
			tieneMaletero = false;
		}
		else if(aux_tieneMaletero.toUpperCase().equals("SI")) {
			tieneMaletero = true;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			tieneMaletero = false;
		}
	}
	
	/**
	* @return esEscolar
	*/
	public boolean getTieneMaletero() {
		return tieneMaletero;
	}
	
	public void brincar() {
		JOptionPane.showMessageDialog(null, "La motocicleta acaba de brincar");
	}
	
	public void aparcar() {
		JOptionPane.showMessageDialog(null, "La motocicleta acaba de aparcar");
	}
	
	/**
	* Metodo heredado de Vehiculo
	* @return cadena
	* @Override
	*/
	public String toString() {
		String tieneMaletero1 = "";
		if (getTieneMaletero() == true) {
			tieneMaletero1 = "Tiene maletero";
		}
		else {
			tieneMaletero1 = "No tiene maletero";
		}
		return "Motocicleta\nMatr\u00edcula: " + getMatricula() + "\nMarca: " + getMarca() + "\nModelo: " + getModelo() + "\nColor: " + getColor() + "\nTiene: " + getKilometros()  + "\nN\u00fameroPlazas: " + getNumPlazas() + "\nPotencia del motor: " + getPotenciaMotor()  + "\n" + tieneMaletero1;  
	}

	@Override
	public void circular() {
		JOptionPane.showMessageDialog(null, "Esto es una motocicleta y las motocicleta pueden circular por carreteras, autov\u00edas y autopistas");
	}
	
	@Override
	public void arrancar() {
		JOptionPane.showMessageDialog(null, "La motocicleta acaba de arrancar");
	}

	@Override
	public void acelerar() {
		JOptionPane.showMessageDialog(null, "La motocicleta esta acelerando");
	}
	
	@Override
	public void frenar() {
		JOptionPane.showMessageDialog(null, "La motocicleta esta frenando");
	}
}