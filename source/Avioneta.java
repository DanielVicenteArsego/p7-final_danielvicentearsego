import javax.swing.JOptionPane;
/**
	* Clase Avioneta
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public class Avioneta extends Vehiculo implements PuedeVolar, PuedeCircular {
	private String aeropuerto;
	private int maxKg;
	
	/**
	* Constructor por defecto
	*/
	public Avioneta() {
		super();
		setAeropuerto("Lake Hood Seaplane Base -- Alaska");
		setMaxKg(300);
	}
	
	/**
	* Constructor con parametros
	* @param aux_marca
	* @param aux_modelo
	* @param @aux_color
	* @param aux_kilometros
	* @param aux_numPuertas
	* @param aux_numPlazas
	* @param aux_aeropuerto
	* @param aux_maxKg
	*/
	public Avioneta(String aux_marca, String aux_modelo, String aux_color, double aux_kilometros, int aux_numPuertas, int aux_numPlazas, String aux_aeropuerto, int aux_maxKg) {
		super(aux_marca, aux_modelo, aux_color, aux_kilometros, aux_numPuertas, aux_numPlazas);
		setAeropuerto(aux_aeropuerto);
		setMaxKg(aux_maxKg);
	}
	
	/**
	* @param aux_aeropuerto = aeropuerto
	*/
	public void setAeropuerto(String aux_aeropuerto) {
		if (!aux_aeropuerto.equals("")) {
			aeropuerto = aux_aeropuerto;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			aeropuerto = "Lake Hood Seaplane Base -- Alaska";
		}
	}
	
	/**
	* @return aeropuerto
	*/
	public String getAeropuerto() {
		return aeropuerto;
	}
	
	/**
	* @param aux_maxKg = maxKg
	*/
	public void setMaxKg(int aux_maxKg) {
		if (aux_maxKg > 0) {
			maxKg = aux_maxKg;
		}
		else {
			maxKg = 0;
		}
	}
	
	/**
	* @return maxKg
	*/
	public int getMaxKg() {
		return maxKg;
	}

	public void despegar() {
		JOptionPane.showMessageDialog(null, "La avioneta acaba de despegar");
	}
	
	public void aterrizar() {
		JOptionPane.showMessageDialog(null, "La avioneta acaba de aterrizar");
	}
	
	@Override
	public void circular() {
		JOptionPane.showMessageDialog(null, "Esto es una avioneta y las avionetas s\u00f3lo pueden circular dentro de los aeropuertos");
	}
	
	@Override
	public void volar() {
		JOptionPane.showMessageDialog(null, "La avioneta puede volar");
	}

	/**
	* Metodo heredado de Vehiculo
	* @return cadena
	* @Override
	*/
	public String toString() {
		return "Avioneta\n" + super.toString() + "Aeropuerto: " + getAeropuerto() + "\nMaxKg" + getMaxKg();  
	}
	
	@Override
	public void arrancar() {
		JOptionPane.showMessageDialog(null, "La avioneta acaba de arrancar");
	}

	@Override
	public void acelerar() {
		JOptionPane.showMessageDialog(null, "La avioneta esta acelerando");
	}
	
	@Override
	public void frenar() {
		JOptionPane.showMessageDialog(null, "La avioneta esta frenando");
	}	
}