import javax.swing.JOptionPane;
import java.util.ArrayList;
/**
	* Clase Parque
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public class Parque {
	public static void main(String [] args) {
		String resp = "";
		int opcion = 0;
		ArrayList<Vehiculo> listaVehiculos = new ArrayList<Vehiculo>();
		do {
			try {
				opcion = Integer.parseInt(JOptionPane.showInputDialog("F\u00e1brica de coches de Daniel Vicente Arsego:\nElige una opci\u00f3n:\n1. Crear un coche\n2. Crear un autob\u00fas\n3. Crear una motocicleta\n4. Crear una avioneta\n5. Crear un yate\n6. Mostrar caracter\u00edsticas de todos los veh\u00edculos del parque\n7. Buscar veh\u00edculo\n8. Borrar veh\u00edculo \n9. Salir del programa"));
				switch (opcion) {
					case 1:
					if (Vehiculo.numVehiculos <= Vehiculo.max_coches) {
						resp = JOptionPane.showInputDialog("(SI) Para crear el coche con caracter\u00edsticas\n(NO) Para crear el coche por defecto"); 
						if (resp.toUpperCase().equals("NO")) {
							Coche coche = new Coche();
							JOptionPane.showMessageDialog(null, coche.toString());
							listaVehiculos.add(coche);
						}
						else if (resp.toUpperCase().equals("SI")) {
							String[] valores = valoresConstructor().split("-");
							Coche coche = new Coche(valores[0], valores[1], valores[2], Integer.parseInt(valores[3]), Integer.parseInt(valores[4]), Integer.parseInt(valores[5]), Integer.parseInt(JOptionPane.showInputDialog("N\u00famero de Airbags: ")), JOptionPane.showInputDialog("Quiere instalar techo solar?"), JOptionPane.showInputDialog("Quiere instalar radio?"));
							JOptionPane.showMessageDialog(null, coche.toString());
							listaVehiculos.add(coche);
						}
						else {
								JOptionPane.showMessageDialog(null, "OPCI\u00d3N INCORRECTA");
								opcion = -1;
						}
					}
					else {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear veh\u00edculos");
							opcion = -1; 
					}
						break;
					case 2:
						if (Vehiculo.numVehiculos <= Vehiculo.max_coches) {
							resp = JOptionPane.showInputDialog("(SI) Para crear el autob\u00fas con caracter\u00edsticas\n(NO) Para crear el autob\u00fas por defecto"); 
							if (resp.toUpperCase().equals("NO")) {
								Autobus autobus = new Autobus();
								JOptionPane.showMessageDialog(null, autobus.toString());
								listaVehiculos.add(autobus);
							}
							else if (resp.toUpperCase().equals("SI")) {
								String[] valores = valoresConstructor().split("-");
								Autobus autobus = new Autobus(valores[0], valores[1], valores[2], Integer.parseInt(valores[3]), Integer.parseInt(valores[4]), Integer.parseInt(valores[5]), JOptionPane.showInputDialog("Introduce el tipo de recorrido: "), JOptionPane.showInputDialog("Es escolar? "));
								JOptionPane.showMessageDialog(null, autobus.toString());
								listaVehiculos.add(autobus);
							}
							else {
								JOptionPane.showMessageDialog(null, "OPCI\u00d3N INCORRECTA");
								opcion = -1;
							}
						}
					else {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear veh\u00edculos");
							opcion = -1; 
					}
						break;
					case 3:
						if (Vehiculo.numVehiculos <= Vehiculo.max_coches) {
							resp = JOptionPane.showInputDialog("(SI) Para crear la motocicleta con caracter\u00edsticas\n(NO) Para crear la motocicleta por defecto"); 
							if (resp.toUpperCase().equals("NO")) {
								Motocicleta motocicleta = new Motocicleta();
								JOptionPane.showMessageDialog(null, motocicleta.toString());
								listaVehiculos.add(motocicleta);
							}
							else if (resp.toUpperCase().equals("SI")) {
								String[] valores = valoresConstructor().split("-");
								Motocicleta motocicleta = new Motocicleta(valores[0], valores[1], valores[2], Integer.parseInt(valores[3]), Integer.parseInt(valores[4]), Integer.parseInt(valores[5]), Integer.parseInt(JOptionPane.showInputDialog("Introduce la potencia del motor: ")), JOptionPane.showInputDialog("Tiene maletero? "));
								JOptionPane.showMessageDialog(null, motocicleta.toString());
								listaVehiculos.add(motocicleta);
							}
							else {
								JOptionPane.showMessageDialog(null, "OPCI\u00d3N INCORRECTA");
								opcion = -1;
							}
					}
					else {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear veh\u00edculos");
							opcion = -1; 
					}
						break;
					case 4:
						if (Vehiculo.numVehiculos <= Vehiculo.max_coches) {
							resp = JOptionPane.showInputDialog("(SI) Para crear la avioneta con caracter\u00edsticas\n(NO) Para crear la avioneta por defecto"); 
							if (resp.toUpperCase().equals("NO")) {
								Avioneta avioneta = new Avioneta();
								JOptionPane.showMessageDialog(null, avioneta.toString());
								listaVehiculos.add(avioneta);
							}
							else if (resp.toUpperCase().equals("SI")) {
								String[] valores = valoresConstructor().split("-");
								Avioneta avioneta = new Avioneta(valores[0], valores[1], valores[2], Integer.parseInt(valores[3]), Integer.parseInt(valores[4]), Integer.parseInt(valores[5]), JOptionPane.showInputDialog("Introduce el aeropuerto: "), Integer.parseInt(JOptionPane.showInputDialog("Introduce el carga m\u00e1xima: ")));
								JOptionPane.showMessageDialog(null, avioneta.toString());
								listaVehiculos.add(avioneta);
							}
							else {
								JOptionPane.showMessageDialog(null, "OPCI\u00d3N INCORRECTA");
								opcion = -1;
							}
						}
						else {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear vehiculos");
							opcion = -1; 
						}
						break;
						
					case 5:
						if (Vehiculo.numVehiculos <= Vehiculo.max_coches) {
							resp = JOptionPane.showInputDialog("(SI) Para crear el yate con caracteristicas\n(NO) Para crear el yate por defecto"); 
							if (resp.toUpperCase().equals("NO")) {
								Yate yate = new Yate();
								JOptionPane.showMessageDialog(null, yate.toString());
								listaVehiculos.add(yate);
							}
							else if (resp.toUpperCase().equals("SI")) {
								String[] valores = valoresConstructor().split("-");
								Yate yate = new Yate(valores[0], valores[1], valores[2], Integer.parseInt(valores[3]), Integer.parseInt(valores[4]), Integer.parseInt(valores[5]), JOptionPane.showInputDialog("Tiene cocina? "), Integer.parseInt(JOptionPane.showInputDialog("Introduce el numero de motores: ")), Integer.parseInt(JOptionPane.showInputDialog("Introduce tamanyo de la eslora: ")));
								JOptionPane.showMessageDialog(null, yate.toString());
								listaVehiculos.add(yate);
							}
							else {
								JOptionPane.showMessageDialog(null, "OPCI\u00d3N INCORRECTA");
								opcion = -1;
							}
						}
						else {
							JOptionPane.showMessageDialog(null, "Ya no se puede crear vehiculos");
							opcion = -1; 
						}
						break;
					case 6:
						if (Vehiculo.numVehiculos == 0) {
							JOptionPane.showMessageDialog(null, "Aun no se ha creado ningun vehiculo");
						}
						else {
							mostrarArray(listaVehiculos);
						}
						break;
					case 7:
						if (Vehiculo.numVehiculos == 0) {
							JOptionPane.showMessageDialog(null, "Aun no se ha creado ningun vehiculo");
						}
						else {
							JOptionPane.showMessageDialog(null, buscaVehiculo(listaVehiculos, JOptionPane.showInputDialog("Introduce la matricula del coche que desea mostrar: ")).toString());
						}
						break;
					case 8:
						if (Vehiculo.numVehiculos == 0) {
							JOptionPane.showMessageDialog(null, "Aun no se ha creado ningun vehiculo");
						}
						else {	
							Vehiculo vehi = buscaVehiculo(listaVehiculos, JOptionPane.showInputDialog("Introduce la matricula del coche que desea borrar: "));
							resp = JOptionPane.showInputDialog("Seguro que quieres borrar el vehiculo?");
							if (resp.toUpperCase().equals("SI")) {
								listaVehiculos.remove(vehi);
								Vehiculo.numVehiculos--;
								JOptionPane.showMessageDialog(null, "VEHICULO BORRADO");
							}	
							else if (resp.toUpperCase().equals("NO")) {
								JOptionPane.showMessageDialog(null, "Vehiculo no borrado");
							}
							else {
								JOptionPane.showMessageDialog(null, "OPCION INCORRECTA");
							}
						}	
						break;
					case 9:
						JOptionPane.showMessageDialog(null, "Estas saliendo...");
						break;
					default:
						JOptionPane.showMessageDialog(null, "Opcion incorrecta!");
						break;
				}
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "OPCION INCORRECTA -- DATOS MAL INTRODUCIDOS");
				opcion = -1;
			}
		} while (opcion != 9);
		
	}
	
	/**
	* @return valores
	* Este metodo pregunta los valores comunes de cada vehiculo y los devuelve
	*/
	public static String valoresConstructor() {
		String valores = JOptionPane.showInputDialog("Introduce la marca: ") + "-" + JOptionPane.showInputDialog("Introduce el modelo: ") + "-" + JOptionPane.showInputDialog("Introduce el color: ") + "-" + JOptionPane.showInputDialog("Introduce los kilometros: ") + "-" + JOptionPane.showInputDialog("Introduce el numero de puertas: ") + "-" + JOptionPane.showInputDialog("Introduce el numero de plazas: ");	
		return valores;
	}
	
	/**
	@param recibe un objeto de tipo vehiculo
	@return devuelve un numero con el tipo de vehiculo de cada objeto vehiculo
	*/
	public static int tipoVehiculo(Vehiculo vehi) {
		int dev = 0;
		if (vehi.getClass().equals(Coche.class)) {
			dev = 1;
		}
		else if (vehi.getClass().equals(Autobus.class)) {
			dev = 2;
		}
		else if (vehi.getClass().equals(Motocicleta.class)) {
			dev = 3;
		}
		else if (vehi.getClass().equals(Avioneta.class)) {
			dev = 4;
		}
		else if (vehi.getClass().equals(Yate.class)) {
			dev = 5;
		}
		else {
			System.out.println(vehi.getClass());
			dev = -1; 
		}
		return dev;
	}
	
	/**
	* @param recibe un objeto de tipo vehiculo
	* muestra todas las caracteristicas de cada vehiculo
	*/
	public static void mostrarArray(ArrayList<Vehiculo> vehi) {
		for (Vehiculo elemento : vehi) {
			switch (tipoVehiculo(elemento)) {
			case 1: 
				Coche coche = (Coche) elemento;
				coche.circular();
				JOptionPane.showMessageDialog(null, coche.toString());
				break;
			case 2: 
				Autobus autobus = (Autobus) elemento;
				autobus.circular();
				JOptionPane.showMessageDialog(null, autobus.toString());
				break;
			case 3:
				Motocicleta motocicleta = (Motocicleta) elemento;
				motocicleta.circular();
				JOptionPane.showMessageDialog(null, motocicleta.toString());
				break;
			case 4:
				Avioneta avioneta = (Avioneta) elemento;
				avioneta.circular();
				avioneta.volar();
				JOptionPane.showMessageDialog(null, avioneta.toString());
				break;
			case 5:
				Yate yate = (Yate) elemento;
				yate.navegar();
				JOptionPane.showMessageDialog(null, yate.toString());
				break;
			default:
				JOptionPane.showMessageDialog(null, "NO SE ENCONTRADO EL VEHICULO");
				break;
			}
		}
	}
	
	/**
		* @return num, devuelve el indice de la posicion del ArrayList buscado
		* @param vehi y matricula, vehi es un ArrayList de tipo Vehiculo y matricula es de tipo String
		* Busca dentro del ArrayList la matricula introducida
	*/
	public static Vehiculo buscaVehiculo(ArrayList<Vehiculo> vehi, String matricula) {
		int num = 0;
		int cont = 0;
		boolean resp = true;
		while (vehi.get(cont) != null && resp) {
			if (vehi.get(cont).getMatricula().equals(matricula)) {
				num = cont;
				resp = false;
			}
			else {
				num = -1;
				cont++;
			}
			if (cont == 5) {
				resp = false;
				cont--;
			}
		}			
		return vehi.get(cont);
	}	
}