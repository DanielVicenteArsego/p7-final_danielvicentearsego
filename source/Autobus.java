import javax.swing.JOptionPane;
/**
	* Clase Autobus
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public class Autobus extends Vehiculo implements PuedeCircular {
	private String tipoRecorrido;
	private boolean esEscolar;
	
	/**
	* Constructor por defecto
	*/
	public Autobus() {
		super();
		setTipoRecorrido("De Valencia a Alicante");
		setEsEscolar("NO");
	}
	
	/**
	* Constructor con parametros
	* @param aux_marca
	* @param aux_modelo
	* @param @aux_color
	* @param aux_kilometros
	* @param aux_numPuertas
	* @param aux_numPlazas
	* @param aux_tipoRecorrido
	* @param aux_esEscolar
	*/
	public Autobus(String aux_marca, String aux_modelo, String aux_color, double aux_kilometros, int aux_numPuertas, int aux_numPlazas, String aux_tipoRecorrido, String aux_esEscolar) {
		super(aux_marca, aux_modelo, aux_color, aux_kilometros, aux_numPuertas, aux_numPlazas);
		setTipoRecorrido(aux_tipoRecorrido);
		setEsEscolar(aux_esEscolar);
	}
	
	/**
	* @param aux_tipoRecorrido = tipoRecorrido
	*/
	public void setTipoRecorrido(String aux_tipoRecorrido) {
		if (!aux_tipoRecorrido.equals("")) {
			tipoRecorrido = aux_tipoRecorrido;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			tipoRecorrido = "De Valencia a Alicante"; 
		}
	}
	
	/**
	* @return tipoRecorrido
	*/
	public String getTipoRecorrido() {
		return tipoRecorrido;
	}
	
	/**
	* @param aux_esEscolar = esEscolar
	*/
	public void setEsEscolar(String aux_esEscolar) {
		if (aux_esEscolar.toUpperCase().equals("NO")) {
			esEscolar = false;
		}
		else if (aux_esEscolar.toUpperCase().equals("SI")) {
			esEscolar = true;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			esEscolar = false;
		}
	}
	
	/**
	* @return esEscolar
	*/
	public boolean getEsEscolar() {
		return esEscolar;
	}
	
	public void abrirPuertas() {
		JOptionPane.showMessageDialog(null, "Se abre las puertas del autob\u00fas");
	}
	
	public void aparcar() {
		JOptionPane.showMessageDialog(null, "El autob\u00fas esta aparcado");
	}
	
	public void circular() {
		JOptionPane.showMessageDialog(null, "Esto es un autob\u00fas y los autobuses pueden circular por carreteras, autov\u00edas y autopistas");
	}
	
	/**
	* Metodo heredado de Vehiculo
	* @return cadena
	*/
	public String toString() {
		String escolar = "";
		if (getEsEscolar() == true) {
			escolar = "Es escolar";
		}
		else {
			escolar = "No es escolar";
		}
		return "Autob\u00fas\n" + super.toString() + "Tipo de recorrido: " + getTipoRecorrido() + "\n" + escolar;  
	}
	
	@Override
	public void arrancar() {
		JOptionPane.showMessageDialog(null, "El Autob\u00fas acaba de arrancar");
	}

	@Override
	public void acelerar() {
		JOptionPane.showMessageDialog(null, "El Autob\u00fas esta acelerando");
	}
	
	@Override
	public void frenar() {
		JOptionPane.showMessageDialog(null, "El Autob\u00fas esta frenando");
	}
}