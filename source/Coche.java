import javax.swing.JOptionPane;
/**
	* Clase Coche
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public class Coche extends Vehiculo {
	private int numAirbags;
	private boolean techoSolar;
	private boolean tieneRadio;
	
	/**
	* Constructor por defecto
	*/
	public Coche() {
		super();
		setNumAirbags(0);
		setTechoSolar("NO");
		setTieneRadio("SI");
	}
	
	/**
	* Constructor con parametros
	* @param aux_marca
	* @param aux_modelo
	* @param @aux_color
	* @param aux_kilometros
	* @param aux_numPuertas
	* @param aux_numPlazas
	* @param aux_numAirbags
	* @param aux_techoSolar
	* @param aux_tieneRadio
	*/
	public Coche(String aux_marca, String aux_modelo, String aux_color, double aux_kilometros, int aux_numPuertas, int aux_numPlazas, int aux_numAirbags, String aux_techoSolar, String aux_tieneRadio) {
		super(aux_marca, aux_modelo, aux_color, aux_kilometros, aux_numPuertas, aux_numPlazas);
		setNumAirbags(aux_numAirbags);
		setTechoSolar(aux_techoSolar);
		setTieneRadio(aux_tieneRadio);
	}
	
	/**
	* @param aux_numAirbags = numAirbags
	*/
	public void setNumAirbags(int aux_numAirbags) {
		if (aux_numAirbags >= 0) {
			numAirbags = aux_numAirbags;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opcion incorrecta o lo has dejado vacio. Se anyadira el dato por defecto");
			numAirbags = 0;
		}
	}
	
	/**
	* @return numAirbags
	*/
	public int getNumAirbags() {
		return numAirbags;
	}
	
	/**
	* @param aux_techoSolar = techoSolar
	*/
	public void setTechoSolar(String aux_techoSolar) {
		if (aux_techoSolar.toUpperCase().equals("NO")) {
			techoSolar = false;
		}
		else if(aux_techoSolar.toUpperCase().equals("SI")) {
			techoSolar = true;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			techoSolar = false;
		}
	}
	
	/**
	* @return techoSolar
	*/
	public boolean getTechoSolar() {
		return techoSolar;
	}
	
	/**
	* @param aux_tieneRadio = tieneRadio
	*/
	public void setTieneRadio(String aux_tieneRadio) {
		if (aux_tieneRadio.toUpperCase().equals("NO")) {
			tieneRadio = false;
		}
		else if(aux_tieneRadio.toUpperCase().equals("SI")) {
			tieneRadio = true;
		}
		else {
			JOptionPane.showMessageDialog(null,"Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			tieneRadio = false; 
		}
	}
	
	/**
	* @return tieneRadio
	*/
	public boolean getTieneRadio() {
		return tieneRadio;
	}
	
	/**
	* @param aux_color = color
	*/
	public void tunear(String aux_color) {
		color = aux_color;
		kilometros = 0;
		if (getTechoSolar() == false) {
			techoSolar = true;
		}
	}
	
	/**
	* Metodo heredado de Vehiculo
	* @return cadena
	* @Override
	*/
	public String toString() {
		String techoSolar1 = "";
		String tieneRadio1 = "";
		if (getTechoSolar() == true) {
			techoSolar1 = "Tiene techoSolar";
		}
		else {
			techoSolar1 = "No tiene techoSolar";
		}
		if (getTieneRadio() == true) {
			tieneRadio1 = "Tiene radio";
		}
		else {
			tieneRadio1 = "No tiene radio";
		}
		return super.toString() + "Numero de airbags: " + getNumAirbags() + "\n" + tieneRadio1 + "\n" + techoSolar1;  
	}
	
	public void aparcar() {
		JOptionPane.showMessageDialog(null, "El coche esta aparcado");
	}
	
	@Override
	public void circular() {
		JOptionPane.showMessageDialog(null, "Esto es un coche y los coches pueden circular por carreteras, autov\u00edas y autopistas");
	}
	
	@Override
	public void arrancar() {
		JOptionPane.showMessageDialog(null, "El coche acaba de arrancar");
	}

	@Override
	public void acelerar() {
		JOptionPane.showMessageDialog(null, "El coche esta acelerando");
	}
	
	@Override
	public void frenar() {
		JOptionPane.showMessageDialog(null, "El coche esta frenando");
	}
}