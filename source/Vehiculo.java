/**
	* Clase Vehiculo
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public abstract class Vehiculo {
	protected String matricula;
	protected String marca;
	protected String modelo;
	protected String color;
	protected double kilometros;
	protected int numPuertas;
	protected int numPlazas;
	static int numVehiculos = 0;
	static final int max_coches = 5;
	
	/** Constructor por defecto */
	public Vehiculo() {
		setMatricula(matAleatoria());
		setMarca("MARCA CHINA");
		setModelo("SIN MODELO");
		setColor("Blanco");
		setKilometros(0);
		setNumPuertas(1);
		setNumPlazas(1);	
		numVehiculos++;
	}
	
	/** Contructor con parametros 
	* @param aux_marca
	* @param aux_modelo
	* @param @aux_color
	* @param aux_kilometros
	* @param aux_numPuertas
	* @param aux_numPlazas
	*/ 
	public Vehiculo(String aux_marca, String aux_modelo, String aux_color, double aux_kilometros, int aux_numPuertas, int aux_numPlazas) {
		setMatricula(matAleatoria());
		setMarca(aux_marca);
		setModelo(aux_modelo);
		setColor(aux_color);
		setKilometros(aux_kilometros);
		setNumPuertas(aux_numPuertas);
		setNumPlazas(aux_numPlazas);
		numVehiculos++;
	}
	
	/**
	* @param aux_matricula = matricula
	*/
	public void setMatricula(String aux_matricula) {
		matricula = aux_matricula;
	}
	
	/**
	* @return matricula
	*/
	public String getMatricula() {
		return matricula;
	}
	
	/**
	* @param aux_marca
	*/
	public void setMarca(String aux_marca) {
		marca = aux_marca;
	}
	
	/**
	* @return marca
	*/
	public String getMarca() {
		return marca;
	}
	
	/**
	* @param aux_modelo = modelo
	*/
	public void setModelo(String aux_modelo) {
		modelo = aux_modelo;
	}
	
	/**
	* @return modelo
	*/
	public String getModelo() {
		return modelo;
	}
	
	/**
	* @param aux_color = color
	*/
	public void setColor(String aux_color) {
		color = aux_color;
	}
	
	/**
	* @return color
	*/
	public String getColor() {
		return color;
	}
	
	/**
	* @param aux_kilometros = kilometros
	*/
	public void setKilometros(double aux_kilometros) {
		if (aux_kilometros >= 0) {
			kilometros = aux_kilometros;
		}
		else {
			kilometros = 0;
		}
	}
	
	/**
	* @return kilometros
	*/
	public double getKilometros() {
		return kilometros;
	}
	
	/**
	* @param aux_numPuertas = numPuertas
	*/
	public void setNumPuertas(int aux_numPuertas) {
		if (aux_numPuertas > 0 && aux_numPuertas <= 30) {
			numPuertas = aux_numPuertas;
		}
		else {
			numPuertas = 1;
		}
	}
	
	/**
	* @return numPuertas
	*/
	public int getNumPuertas() {
		return numPuertas;
	}
	
	/**
	* @param aux_numPlazas = numPlazas
	*/
	public void setNumPlazas(int aux_numPlazas) {
		if (aux_numPlazas > 0 && aux_numPlazas <= 250) {
			numPlazas = aux_numPlazas;
		}
		else {
			numPlazas = 1;
		}
	}
	
	/**
	* @return numPlazas
	*/
	public int getNumPlazas() {
		return numPlazas;
	}
	
	/**
	* @return datos
	* metodo abstract arrancar
	*/
			
	
	/**
	* @return datos
	* metodo abstract acelerar
	*/
	public abstract void acelerar();
	
	/**
	* @return datos
	* metodo abstract frenar
	*/
	public abstract void frenar();
	
	/**
	* @return datos
	* metodo abstract frenar
	*/
	public String toString() {
		return "Matr\u00edcula: " + getMatricula() + "\nMarca: " + getMarca() + "\nModelo: " + getModelo() + "\nColor: " + getColor() + "\nTiene: " + getKilometros() + " km" + "\nN\u00famero de puertas: " + getNumPuertas() + "\nN\u00fameroPlazas: " + getNumPlazas() + "\n";
	}
	
	/** @return matriculaAle, devuelve una matricula aleatoria */
	public String matAleatoria() {
		double numero1 = Math.random() * 100000;
		int numero2 = (int)numero1;
		String matriculaAle = Integer.toString(numero2);
		return matriculaAle;
	}
}