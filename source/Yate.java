import javax.swing.JOptionPane;
/**
	* Clase Yate
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
public class Yate extends Vehiculo implements PuedeNavegar {
	private boolean tieneCocina;
	private int numMotores;
	private int metrosEslora;
	
	/**
	* Constructor por defecto
	*/
	public Yate() {
		super();
		setTieneCocina("NO");
		setNumMotores(1);
		setMetrosEslora(5);
	}
	
	/**
	* Constructor con parametros
	* @param aux_marca
	* @param aux_modelo
	* @param @aux_color
	* @param aux_kilometros
	* @param aux_numPuertas
	* @param aux_numPlazas
	* @param aux_tieneCocina
	* @param aux_numMotores
	* @param aux_metrosEslora
	*/
	public Yate(String aux_marca, String aux_modelo, String aux_color, double aux_kilometros, int aux_numPuertas, int aux_numPlazas, String aux_tieneCocina, int aux_numMotores, int aux_metrosEslora) {
		super(aux_marca, aux_modelo, aux_color, aux_kilometros, aux_numPuertas, aux_numPlazas);
		setTieneCocina(aux_tieneCocina);
		setNumMotores(aux_numMotores);
		setMetrosEslora(aux_metrosEslora);
	}
	
	/**
	* @param aux_tieneCocina = tieneCocina
	*/
	public void setTieneCocina(String aux_tieneCocina) {
		if (aux_tieneCocina.toUpperCase().equals("NO")) {
			tieneCocina = false;
		}
		else if(aux_tieneCocina.toUpperCase().equals("SI")) {
			tieneCocina = true;
		}
		else {
			JOptionPane.showMessageDialog(null, "Opci\u00f3n incorrecta o lo has dejado vac\u00edo. Se a\u00f1adira el dato por defecto");
			tieneCocina = false;
		}
	}
	
	/**
	* @return tieneCocina
	*/
	public boolean getTieneCocina() {
		return tieneCocina;
	}
	
	/**
	* @param aux_numMotores = numMotores
	*/
	public void setNumMotores(int aux_numMotores) {
		if (aux_numMotores > 0) {
			numMotores = aux_numMotores;
		}
		else {
			numMotores = 1;
		}
	}
	
	/**
	* @return numMotores
	*/
	public int getNumMotores() {
		return numMotores;
	}
	
	/**
	* @param aux_metrosEslora = metrosEslora
	*/
	public void setMetrosEslora(int aux_metrosEslora) {
		if (aux_metrosEslora > 0 && aux_metrosEslora < 250) {
			metrosEslora = aux_metrosEslora;
		}
		else {
			metrosEslora = 5;
		}
	}
	
	/**
	* @return metrosEslora
	*/
	public int getMetrosEslora() {
		return metrosEslora;
	}
	
	public void zarpar() {
		JOptionPane.showMessageDialog(null, "El yate acaba de zarpar");
	}
	
	public void atracar() {
		JOptionPane.showMessageDialog(null, "El yate acaba de atracar");
	}
	
	/**
	* Metodo heredado de Vehiculo
	* @return cadena
	* @Override
	*/
	public String toString() {
		String tieneCocina1 = "";
		if (getTieneCocina() == true) {
			tieneCocina1 = "Tiene cocina";
		}
		else {
			tieneCocina1 = "No tiene cocina";
		}
		return "Yate\n" + super.toString() + tieneCocina1  + "\nTiene: " + getNumMotores() + " motores\nMetros eslora: " + getMetrosEslora();  
	}

	@Override
	public void navegar() {
		JOptionPane.showMessageDialog(null, "Esto es un yate y los yates pueden circular por el mar");
	}
	
	@Override
	public void arrancar() {
		JOptionPane.showMessageDialog(null, "El yeta acaba de arrancar");
	}

	@Override
	public void acelerar() {
		JOptionPane.showMessageDialog(null, "El yate esta acelerando");
	}
	
	@Override
	public void frenar() {
		JOptionPane.showMessageDialog(null, "El yate esta frenando");
	}
}