/**
	* interface PuedeVolar
	* @author Daniel Vicente Arsego
	* @version 1.0
*/
interface PuedeVolar {
	public void volar();
}